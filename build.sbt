name := "elt-test"

version := "0.1"

scalaVersion := "2.12.6"

val finchVersion = "0.19.0"
val circeVersion = "0.9.3"

libraryDependencies ++= Seq(
  "com.github.finagle" %% "finch-core",
  "com.github.finagle" %% "finch-circe",
  "com.github.finagle" %% "finch-test"
).map(_ % finchVersion)

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)

libraryDependencies += "com.twitter" %% "twitter-server" % "18.5.0"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"