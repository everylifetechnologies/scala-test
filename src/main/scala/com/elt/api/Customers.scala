package com.elt.api

import com.elt.api.utils.ApiUtils
import com.elt.model.{Customer, CustomerNotFound}
import com.elt.storage.CustomerStorage
import io.finch.syntax.get
import io.finch.{Ok, _}
import io.finch.syntax.scalaFutures._
import shapeless._

import scala.concurrent.ExecutionContext

object Customers extends ApiUtils {
  implicit val ec: ExecutionContext = ExecutionContext.global

  val customersEndpoint: Endpoint[HNil] = "api" :: "customers"
  val customerEndpoint: Endpoint[Long] = customersEndpoint :: path[Long]

  def listApi: Endpoint[List[Customer]] = get(customersEndpoint) {
    CustomerStorage.getAll.map(Ok)
  }

  def getApi: Endpoint[Customer] = get(customerEndpoint) { customerId: Long =>
    CustomerStorage.getById(customerId).map {
      _.fold[Output[Customer]](throw CustomerNotFound(customerId))(d => Ok(d))
    }
  }

  def getWithDocumentsApi: Endpoint[String] =
    get(customerEndpoint :: "withDocs" :: booleanParamOption("includeLocked")) { (customerId: Long, includeLocked: Boolean) =>
      // todo create a service function somewhere which would do the job and call it here, feel free to change this Endpoint's type
      Ok("not implemented")
    }
}
