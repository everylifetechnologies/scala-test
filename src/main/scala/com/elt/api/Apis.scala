package com.elt.api

import com.elt.model.{CustomerNotFound, DocumentNotFound}
import com.twitter.finagle.Service
import com.twitter.finagle.http.{Request, Response}
import io.finch.Application
import io.finch.{NotFound, InternalServerError}
import io.finch.circe._
import io.circe.generic.auto._

object Apis {
  def makeService: Service[Request, Response] = (
      Documents.listApi :+: Documents.getApi :+: Customers.listApi :+: Customers.getApi :+: Customers.getWithDocumentsApi
    ).handle({
      case e @ (_: CustomerNotFound | _: DocumentNotFound) => NotFound(e.asInstanceOf[Exception])
      case e: Exception => InternalServerError(e)
    }).toServiceAs[Application.Json]
}
