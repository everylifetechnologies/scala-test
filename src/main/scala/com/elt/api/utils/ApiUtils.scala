package com.elt.api.utils

import io.finch.{Endpoint, paramOption}

trait ApiUtils {
  def booleanParamOption(name: String): Endpoint[Boolean] = paramOption[String](name).map(optStringToBoolean)

  private def optStringToBoolean(input: Option[String]): Boolean = {
    // todo fix this
    // if there is a string and it's "true" or "" then return true, for all other cases return false
    false
  }
}
