package com.elt.api

import com.elt.model.{Document, DocumentNotFound}
import com.elt.storage.DocumentStorage
import io.finch.{Ok, _}
import io.finch.syntax.get
import io.finch.syntax.scalaFutures._
import shapeless.HNil

import scala.concurrent.ExecutionContext

object Documents {
  implicit val ec: ExecutionContext = ExecutionContext.global

  val documentsEndpoint: Endpoint[HNil] = "api" :: "documents"
  val documentEndpoint: Endpoint[Long] = documentsEndpoint :: path[Long]

  def listApi: Endpoint[List[Document]] = get(documentsEndpoint) {
    DocumentStorage.getAll.map(Ok)
  }

  def getApi: Endpoint[Document] = get(documentEndpoint) { docId: Long =>
    DocumentStorage.getById(docId).map(_.fold[Output[Document]](throw DocumentNotFound(docId))(d => Ok(d)))
  }
}
