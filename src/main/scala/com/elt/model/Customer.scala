package com.elt.model

case class Customer(id: Long, title: String, firstname: String, surname: String)

case class CustomerNotFound(id: Long) extends Exception {
  override def getMessage: String = s"Customer with id $id not found"
}