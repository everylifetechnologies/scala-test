package com.elt.model

case class Document(id: Long, content: String, isLocked: Boolean, customerId: Long)

case class DocumentNotFound(id: Long) extends Exception {
  override def getMessage: String = s"Document with id $id not found"
}
