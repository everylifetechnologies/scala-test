package com.elt.storage

import com.elt.model.Customer

import scala.collection.mutable
import scala.concurrent.Future

object CustomerStorage extends AsyncStorage[Customer] {
  private val customers = mutable.Map(
    1L -> Customer(1L, "Mr", "First", "Customer"),
    2L -> Customer(2L, "Mr", "Second", "Customer"),
    3L -> Customer(3L, "Mr", "Third", "Customer")
  )

  def getAll: Future[List[Customer]] = Future.successful(customers.values.toList)

  def getById(id: Long): Future[Option[Customer]] = Future.successful(customers.get(id))
}
