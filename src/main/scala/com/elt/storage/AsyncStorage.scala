package com.elt.storage

import scala.concurrent.Future

trait AsyncStorage[A] {
  def getAll: Future[List[A]]
  def getById(id: Long): Future[Option[A]]
}
