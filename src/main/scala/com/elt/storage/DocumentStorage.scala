package com.elt.storage

import com.elt.model.Document

import scala.collection.mutable
import scala.concurrent.Future

object DocumentStorage extends AsyncStorage[Document] {
  private val documents = mutable.Map(
    1L -> Document(1L, "Document 1", isLocked = false, customerId = 1),
    2L -> Document(2L, "Document 2", isLocked = true, customerId = 2),
    3L -> Document(3L, "Document 3", isLocked = false, customerId = 2)
  )

  def getAll: Future[List[Document]] = Future.successful(documents.values.toList)

  def getById(id: Long): Future[Option[Document]] = Future.successful(documents.get(id))
}
