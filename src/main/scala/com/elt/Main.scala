package com.elt

import com.elt.api.Apis
import com.twitter.finagle.Http
import com.twitter.util.Await
import com.twitter.server.TwitterServer

object Main extends TwitterServer {
  def main() {
    val server = Http.server.serve(":8080", Apis.makeService)

    onExit { server.close() }

    Await.ready(adminHttpServer)
  }
}
