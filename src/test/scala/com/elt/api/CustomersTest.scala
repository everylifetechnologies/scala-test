package com.elt.api

import com.elt.model.CustomerNotFound
import io.finch.Input
import org.scalatest.FlatSpec

class CustomersTest extends FlatSpec {
  "Customers list API" should "return all customers" in {
    val result = Customers.listApi(Input.get("/api/customers")).awaitValueUnsafe()
    assert(result.exists(_.length == 3))
  }

  "Customer get API" should "return specific customer if found" in {
    val result = Customers.getApi(Input.get("/api/customers/1")).awaitValueUnsafe()
    assert(result.exists(_.id == 1))
  }

  it should "throw an exception if customer not found" in {
    val result = Customers.getApi(Input.get("/api/customers/-1")).awaitValue()
    assert(result.exists(_.throwable.isInstanceOf[CustomerNotFound]))
  }

  // todo write tests for Customers.getWithDocumentsApi
}
