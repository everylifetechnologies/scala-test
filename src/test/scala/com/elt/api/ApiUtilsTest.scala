package com.elt.api

import com.elt.api.utils.ApiUtils
import io.finch.{Endpoint, Input, Ok}
import io.finch.syntax.get
import org.scalatest.FlatSpec

class ApiUtilsTest extends FlatSpec {
  private val apiUtils = new ApiUtils {}

  // todo this should pass if booleanParamOption is implemented correctly
  "Boolean param" should "be resolved correctly if it's set and default to true if present with no value" in {
    val paramName = "myFlag"
    val endpoint: Endpoint[Boolean] = get("api" :: apiUtils.booleanParamOption(paramName)) {boolParam: Boolean => Ok(boolParam)}

    assert(endpoint(Input.get(s"/api?$paramName=true")).awaitValueUnsafe().contains(true))
    assert(endpoint(Input.get(s"/api?$paramName=false")).awaitValueUnsafe().contains(false))
    assert(endpoint(Input.get(s"/api?$paramName")).awaitValueUnsafe().contains(true))
    assert(endpoint(Input.get(s"/api")).awaitValueUnsafe().contains(false))
  }
}
