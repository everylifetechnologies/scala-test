package com.elt.api

import com.elt.model.DocumentNotFound
import io.finch.Input
import org.scalatest.FlatSpec

class DocumentsTest extends FlatSpec {
  "Documents list API" should "return all documents" in {
    val result = Documents.listApi(Input.get("/api/documents")).awaitValueUnsafe()
    assert(result.exists(_.length == 3))
  }

  "Document get API" should "return specific document if found" in {
    val result = Documents.getApi(Input.get("/api/documents/1")).awaitValueUnsafe()
    assert(result.exists(_.id == 1))
  }

  it should "throw an exception if document not found" in {
    val result = Documents.getApi(Input.get("/api/documents/-1")).awaitValue()
    assert(result.exists(_.throwable.isInstanceOf[DocumentNotFound]))
  }
}

