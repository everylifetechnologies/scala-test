**ELT Interview task**


*You are given a simple project which exposes the following APIs:*
- Get documents [`/api/documents`]
- Get document by id [`/api/documents/:documentId`]
- Get customers [`/api/customers`]
- Get customer by id [`/api/customers/:customerId`]

*Each document belongs to a certain customer and thus has a `customerId`.
Also, each document has an `isLocked` boolean flag.*

Your task is to implement an API which would return customer information combined with associated documents information
(if there are any documents belonging to a customer). The API endpoint should be `/api/customers/:customerId/withDocs`,
 it should also take a query parameter named `includeLocked`.
 
 By default, only non-locked documents should be included in the response unless `includeLocked` parameter is present with no value or is set to true,
e.g. `/api/customers/:customerId/withDocs?includeLocked` and `/api/customers/:customerId/withDocs?includeLocked=true` should both include locked documents.

The API is already declared in `com.elt.api.Customers` but is not implemented.
There is a failing test in `ApiUtilsTest` (because the function being tested is not implemented correctly) which you would need to fix
 and you should feel free to write any additional tests for your new API.
 
Feel free to create any additional classes/files you think are needed.